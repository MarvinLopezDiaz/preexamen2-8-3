const http = require('http');
const express = require('express');
const bodyparser = require('body-parser');

const misRuta = require("./router/index");
const path = require("path");

const app = express();

app.set("view engine","ejs");
app.use(express.static(__dirname + '/public'));
app.use(bodyparser.urlencoded({extended:true}));

app.engine("html", require("ejs").renderFile);

app.use(misRuta);

const puerto = 500;

app.listen(puerto,()=>{
    console.log("Iniciando puerto");
});

//La pagina de error va al final de los gets

app.use((req,res,next)=>{
    res.status(404).sendFile(__dirname + '/public/error.html');
});

