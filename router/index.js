const express = require("express");
const router = express.Router();

router.get("/", (req, res) => {
	const params = {
		nBoletos: req.query.nBoletos,
		destino: req.query.destino,
		nombre: req.query.nombre,
		edad: parseInt(req.query.edad),
		tipoViaje: req.query.tipoViaje,
		precio: parseFloat(req.query.precio)
	};

	res.render("index.html", params);
});

router.post("/", (req, res) => {
	const params = {
		nBoletos: req.body.nBoletos,
		destino: req.body.destino,
		nombre: req.body.nombre,
		edad: parseInt(req.body.edad),
		tipoViaje: req.body.tipoViaje,
		precio: parseFloat(req.body.precio)
	};

	res.render("index.html", params);
});

module.exports = router;